///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nene.cpp
/// @version 1.0
///
/// Exports data about all nene birds
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   27_02_2021
///////////////////////////////////////////////////////////////////////////////
#include <string>
#include <iostream>

#include "nene.hpp"

using namespace std;

namespace animalfarm {

Nene::Nene( string tagId, enum Color newColor, enum Gender newGender ) {
   tagid = tagId;
   species = "Branta sandvicensis";
   gender = newGender;
   featherColor = newColor;
   isMigratory = true;
}


const string Nene::speak() {
   return string( "Nay, nay" );
}


void Nene::printInfo() {
   cout << "Nene" << endl;
   cout << "   Tag ID = [" << tagid << "]" << endl;
   Bird::printInfo();
}

}
