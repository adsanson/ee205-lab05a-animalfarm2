///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file palila.hpp
/// @version 1.0
///
/// Exports data about all palila
///
/// @author Albert D'Sanson <adsanson@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   27_02_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once

#include <string>

#include "bird.hpp"

using namespace std;


namespace animalfarm {

class Palila : public Bird {
public:

   string wherefound;

   Palila( string whereFound, enum Color newColor, enum Gender newGender );

   void printInfo();
};

}
